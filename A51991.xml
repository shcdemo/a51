<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A51991">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Lord Churchill's letter to the King</title>
    <title>Letter to the king</title>
    <author>Marlborough, John Churchill, Duke of, 1650-1722.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A51991 of text R16811 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing M691). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A51991</idno>
    <idno type="STC">Wing M691</idno>
    <idno type="STC">ESTC R16811</idno>
    <idno type="EEBO-CITATION">13369835</idno>
    <idno type="OCLC">ocm 13369835</idno>
    <idno type="VID">99294</idno>
    <idno type="PROQUESTGOID">2248521200</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A51991)</note>
    <note>Transcribed from: (Early English Books Online ; image set 99294)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 466:6)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Lord Churchill's letter to the King</title>
      <title>Letter to the king</title>
      <author>Marlborough, John Churchill, Duke of, 1650-1722.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[S.l. :</pubPlace>
      <date>1688]</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Reproduction of original in Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Revolution of 1688 -- Sources.</term>
     <term>Broadsides -- England -- London -- 17th century</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The Lord Churchill's letter to the King.</ep:title>
    <ep:author>Marlborough, John Churchill, Duke of, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>310</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-12</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2008-02</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A51991-t">
  <body xml:id="A51991-e0">
   <div type="text" xml:id="A51991-e10">
    <pb facs="tcp:99294:1" rend="simple:additions" xml:id="A51991-001-a"/>
    <head xml:id="A51991-e20">
     <w lemma="the" pos="d" xml:id="A51991-001-a-0010">The</w>
     <w lemma="lord" pos="n1" xml:id="A51991-001-a-0020">Lord</w>
     <w lemma="churchill" pos="nng1" rend="hi-apo-plain" xml:id="A51991-001-a-0030">Churchill's</w>
     <hi xml:id="A51991-e40">
      <w lemma="letter" pos="n1" xml:id="A51991-001-a-0050">LETTER</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-0060">to</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-0070">the</w>
     <hi xml:id="A51991-e50">
      <w lemma="king" pos="n1" xml:id="A51991-001-a-0080">KING</w>
      <pc unit="sentence" xml:id="A51991-001-a-0090">.</pc>
     </hi>
    </head>
    <opener xml:id="A51991-e60">
     <salute xml:id="A51991-e70">
      <w lemma="sir" pos="n1" xml:id="A51991-001-a-0100">SIR</w>
      <pc xml:id="A51991-001-a-0110">,</pc>
     </salute>
    </opener>
    <p xml:id="A51991-e80">
     <w lemma="since" pos="acp" xml:id="A51991-001-a-0120">SInce</w>
     <w lemma="man" pos="n2" xml:id="A51991-001-a-0130">Men</w>
     <w lemma="be" pos="vvb" xml:id="A51991-001-a-0140">are</w>
     <w lemma="seldom" pos="av" xml:id="A51991-001-a-0150">seldom</w>
     <w lemma="suspect" pos="vvn" xml:id="A51991-001-a-0160">suspected</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-0170">of</w>
     <w lemma="sincerity" pos="n1" xml:id="A51991-001-a-0180">Sincerity</w>
     <w lemma="when" pos="crq" xml:id="A51991-001-a-0190">when</w>
     <w lemma="they" pos="pns" xml:id="A51991-001-a-0200">they</w>
     <w lemma="act" pos="vvi" xml:id="A51991-001-a-0210">act</w>
     <w lemma="contrary" pos="j" xml:id="A51991-001-a-0220">contrary</w>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-0230">to</w>
     <w lemma="their" pos="po" xml:id="A51991-001-a-0240">their</w>
     <w lemma="interest" pos="n2" xml:id="A51991-001-a-0250">Interests</w>
     <pc xml:id="A51991-001-a-0260">;</pc>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-0270">and</w>
     <w lemma="though" pos="cs" reg="though" xml:id="A51991-001-a-0280">thô</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-0290">my</w>
     <w lemma="dutiful" pos="j" xml:id="A51991-001-a-0300">dutiful</w>
     <w lemma="behaviour" pos="n1" xml:id="A51991-001-a-0310">Behaviour</w>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-0320">to</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-0330">your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-0340">Majesty</w>
     <w lemma="in" pos="acp" xml:id="A51991-001-a-0350">in</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-0360">the</w>
     <w lemma="worst" pos="js" xml:id="A51991-001-a-0370">worst</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-0380">of</w>
     <w lemma="time" pos="n2" xml:id="A51991-001-a-0390">times</w>
     <pc xml:id="A51991-001-a-0400">,</pc>
     <pc join="right" xml:id="A51991-001-a-0410">(</pc>
     <w lemma="for" pos="acp" xml:id="A51991-001-a-0420">for</w>
     <w lemma="which" pos="crq" xml:id="A51991-001-a-0430">which</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-0440">I</w>
     <w lemma="acknowledge" pos="vvb" reg="acknowledge" xml:id="A51991-001-a-0450">acknowledg</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-0460">my</w>
     <w lemma="poor" pos="j" xml:id="A51991-001-a-0470">poor</w>
     <w lemma="service" pos="n2" xml:id="A51991-001-a-0480">Services</w>
     <w lemma="much" pos="d" xml:id="A51991-001-a-0490">much</w>
     <w lemma="over-paid" pos="j" xml:id="A51991-001-a-0500">over-paid</w>
     <pc xml:id="A51991-001-a-0510">)</pc>
     <w lemma="may" pos="vmb" xml:id="A51991-001-a-0520">may</w>
     <w lemma="not" pos="xx" xml:id="A51991-001-a-0530">not</w>
     <w lemma="be" pos="vvi" xml:id="A51991-001-a-0540">be</w>
     <w lemma="sufficient" pos="j" xml:id="A51991-001-a-0550">sufficient</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-0560">to</w>
     <w lemma="incline" pos="vvi" reg="incline" xml:id="A51991-001-a-0570">encline</w>
     <w lemma="you" pos="pn" xml:id="A51991-001-a-0580">You</w>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-0590">to</w>
     <w lemma="a" pos="d" xml:id="A51991-001-a-0600">a</w>
     <w lemma="charitable" pos="j" xml:id="A51991-001-a-0610">charitable</w>
     <w lemma="interpretation" pos="n1" xml:id="A51991-001-a-0620">Interpretation</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-0630">of</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-0640">my</w>
     <w lemma="action" pos="n2" xml:id="A51991-001-a-0650">Actions</w>
     <pc xml:id="A51991-001-a-0660">,</pc>
     <w lemma="yet" pos="av" xml:id="A51991-001-a-0670">yet</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-0680">I</w>
     <w lemma="hope" pos="vvb" xml:id="A51991-001-a-0690">hope</w>
     <pc xml:id="A51991-001-a-0700">,</pc>
     <w lemma="the" pos="d" xml:id="A51991-001-a-0710">the</w>
     <w lemma="great" pos="j" xml:id="A51991-001-a-0720">great</w>
     <w lemma="advantage" pos="n1" xml:id="A51991-001-a-0730">Advantage</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-0740">I</w>
     <w lemma="enjoy" pos="vvb" xml:id="A51991-001-a-0750">enjoy</w>
     <w lemma="under" pos="acp" xml:id="A51991-001-a-0760">under</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-0770">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-0780">Majesty</w>
     <pc xml:id="A51991-001-a-0790">,</pc>
     <w lemma="which" pos="crq" xml:id="A51991-001-a-0800">which</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-0810">I</w>
     <w lemma="can" pos="vmb" xml:id="A51991-001-a-0820">can</w>
     <w lemma="never" pos="avx" xml:id="A51991-001-a-0830">never</w>
     <w lemma="expect" pos="vvi" xml:id="A51991-001-a-0840">expect</w>
     <w lemma="in" pos="acp" xml:id="A51991-001-a-0850">in</w>
     <w lemma="any" pos="d" xml:id="A51991-001-a-0860">any</w>
     <w lemma="other" pos="d" xml:id="A51991-001-a-0870">other</w>
     <w lemma="change" pos="n1" xml:id="A51991-001-a-0880">Change</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-0890">of</w>
     <w lemma="government" pos="n1" xml:id="A51991-001-a-0900">Government</w>
     <pc xml:id="A51991-001-a-0910">,</pc>
     <w lemma="may" pos="vmb" xml:id="A51991-001-a-0920">may</w>
     <w lemma="reasonable" pos="av-j" xml:id="A51991-001-a-0930">reasonably</w>
     <w lemma="convince" pos="vvi" xml:id="A51991-001-a-0940">convince</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-0950">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-0960">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-0970">and</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-0980">the</w>
     <w lemma="world" pos="n1" xml:id="A51991-001-a-0990">World</w>
     <pc xml:id="A51991-001-a-1000">,</pc>
     <w lemma="that" pos="cs" xml:id="A51991-001-a-1010">that</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-1020">I</w>
     <w lemma="be" pos="vvm" xml:id="A51991-001-a-1030">am</w>
     <w lemma="act" pos="vvn" xml:id="A51991-001-a-1040">acted</w>
     <w lemma="by" pos="acp" xml:id="A51991-001-a-1050">by</w>
     <w lemma="a" pos="d" xml:id="A51991-001-a-1060">a</w>
     <w lemma="high" pos="jc" xml:id="A51991-001-a-1070">higher</w>
     <w lemma="principle" pos="n1" xml:id="A51991-001-a-1080">Principle</w>
     <pc xml:id="A51991-001-a-1090">,</pc>
     <w lemma="when" pos="crq" xml:id="A51991-001-a-1100">when</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-1110">I</w>
     <w lemma="offer" pos="vvb" xml:id="A51991-001-a-1120">offer</w>
     <w lemma="that" pos="d" xml:id="A51991-001-a-1130">that</w>
     <w lemma="violence" pos="n1" xml:id="A51991-001-a-1140">Violence</w>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-1150">to</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-1160">my</w>
     <w lemma="inclination" pos="n1" xml:id="A51991-001-a-1170">Inclination</w>
     <pc xml:id="A51991-001-a-1180">,</pc>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-1190">and</w>
     <w lemma="interest" pos="n1" xml:id="A51991-001-a-1200">Interest</w>
     <pc xml:id="A51991-001-a-1210">,</pc>
     <w lemma="as" pos="acp" xml:id="A51991-001-a-1220">as</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-1230">to</w>
     <w lemma="desert" pos="vvi" xml:id="A51991-001-a-1240">desert</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-1250">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-1260">Majesty</w>
     <w lemma="at" pos="acp" xml:id="A51991-001-a-1270">at</w>
     <w lemma="a" pos="d" xml:id="A51991-001-a-1280">a</w>
     <w lemma="time" pos="n1" xml:id="A51991-001-a-1290">time</w>
     <w lemma="when" pos="crq" xml:id="A51991-001-a-1300">when</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-1310">Your</w>
     <w lemma="affair" pos="n2" xml:id="A51991-001-a-1320">Affairs</w>
     <w lemma="seem" pos="vvb" xml:id="A51991-001-a-1330">seem</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-1340">to</w>
     <w lemma="challenge" pos="vvi" xml:id="A51991-001-a-1350">challenge</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-1360">the</w>
     <w lemma="strict" pos="js" xml:id="A51991-001-a-1370">strictest</w>
     <w lemma="obedience" pos="n1" xml:id="A51991-001-a-1380">Obedience</w>
     <w lemma="from" pos="acp" xml:id="A51991-001-a-1390">from</w>
     <w lemma="all" pos="d" xml:id="A51991-001-a-1400">all</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-1410">Your</w>
     <w lemma="subject" pos="n2" xml:id="A51991-001-a-1420">Subjects</w>
     <pc xml:id="A51991-001-a-1430">,</pc>
     <w lemma="much" pos="av-d" xml:id="A51991-001-a-1440">much</w>
     <w lemma="more" pos="avc-d" xml:id="A51991-001-a-1450">more</w>
     <w lemma="from" pos="acp" xml:id="A51991-001-a-1460">from</w>
     <w lemma="one" pos="pi" xml:id="A51991-001-a-1470">one</w>
     <w lemma="who" pos="crq" xml:id="A51991-001-a-1480">who</w>
     <w lemma="lie" pos="vvz" xml:id="A51991-001-a-1490">lies</w>
     <w lemma="under" pos="acp" xml:id="A51991-001-a-1500">under</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-1510">the</w>
     <w lemma="great" pos="js" xml:id="A51991-001-a-1520">greatest</w>
     <w lemma="personal" pos="j" xml:id="A51991-001-a-1530">personal</w>
     <w lemma="obligation" pos="n2" xml:id="A51991-001-a-1540">Obligations</w>
     <w lemma="imaginable" pos="j" xml:id="A51991-001-a-1550">imaginable</w>
     <w lemma="to" pos="acp" xml:id="A51991-001-a-1560">to</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-1570">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-1580">Majesty</w>
     <pc unit="sentence" xml:id="A51991-001-a-1590">.</pc>
     <w lemma="this" pos="d" xml:id="A51991-001-a-1600">This</w>
     <pc xml:id="A51991-001-a-1610">,</pc>
     <w lemma="sir" pos="n1" xml:id="A51991-001-a-1620">Sir</w>
     <pc xml:id="A51991-001-a-1630">,</pc>
     <w lemma="can" pos="vmd" xml:id="A51991-001-a-1640">could</w>
     <w lemma="proceed" pos="vvi" xml:id="A51991-001-a-1650">proceed</w>
     <w lemma="from" pos="acp" xml:id="A51991-001-a-1660">from</w>
     <w lemma="nothing" pos="pix" xml:id="A51991-001-a-1670">nothing</w>
     <w lemma="but" pos="acp" xml:id="A51991-001-a-1680">but</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-1690">the</w>
     <w lemma="inviolable" pos="j" xml:id="A51991-001-a-1700">inviolable</w>
     <w lemma="dictate" pos="vvz" xml:id="A51991-001-a-1710">Dictates</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-1720">of</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-1730">my</w>
     <w lemma="conscience" pos="n1" xml:id="A51991-001-a-1740">Conscience</w>
     <pc xml:id="A51991-001-a-1750">,</pc>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-1760">and</w>
     <w lemma="necessary" pos="j" xml:id="A51991-001-a-1770">necessary</w>
     <w lemma="concern" pos="vvi" xml:id="A51991-001-a-1780">concern</w>
     <w lemma="for" pos="acp" xml:id="A51991-001-a-1790">for</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-1800">my</w>
     <w lemma="religion" pos="n1" xml:id="A51991-001-a-1810">Religion</w>
     <pc join="right" xml:id="A51991-001-a-1820">(</pc>
     <w lemma="which" pos="crq" xml:id="A51991-001-a-1830">which</w>
     <w lemma="no" pos="dx" xml:id="A51991-001-a-1840">no</w>
     <w lemma="good" pos="j" xml:id="A51991-001-a-1850">good</w>
     <w lemma="man" pos="n1" xml:id="A51991-001-a-1860">Man</w>
     <w lemma="can" pos="vmb" xml:id="A51991-001-a-1870">can</w>
     <w lemma="oppose" pos="vvi" xml:id="A51991-001-a-1880">oppose</w>
     <pc xml:id="A51991-001-a-1890">)</pc>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-1900">and</w>
     <w lemma="with" pos="acp" xml:id="A51991-001-a-1910">with</w>
     <w lemma="which" pos="crq" xml:id="A51991-001-a-1920">which</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-1930">I</w>
     <w lemma="be" pos="vvm" xml:id="A51991-001-a-1940">am</w>
     <w lemma="instruct" pos="vvn" xml:id="A51991-001-a-1950">instructed</w>
     <pc xml:id="A51991-001-a-1960">,</pc>
     <w lemma="nothing" pos="pix" xml:id="A51991-001-a-1970">nothing</w>
     <w lemma="ought" pos="vmd" xml:id="A51991-001-a-1980">ought</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-1990">to</w>
     <w lemma="come" pos="vvi" xml:id="A51991-001-a-2000">come</w>
     <w lemma="in" pos="acp" xml:id="A51991-001-a-2010">in</w>
     <w lemma="competition" pos="n1" xml:id="A51991-001-a-2020">competition</w>
     <pc xml:id="A51991-001-a-2030">;</pc>
     <w lemma="heaven" pos="n1" xml:id="A51991-001-a-2040">Heaven</w>
     <w lemma="know" pos="vvz" xml:id="A51991-001-a-2050">knows</w>
     <w lemma="with" pos="acp" xml:id="A51991-001-a-2060">with</w>
     <w lemma="what" pos="crq" xml:id="A51991-001-a-2070">what</w>
     <w lemma="partiality" pos="n1" xml:id="A51991-001-a-2080">partiality</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-2090">my</w>
     <w lemma="dutiful" pos="j" xml:id="A51991-001-a-2100">dutiful</w>
     <w lemma="opinion" pos="n1" xml:id="A51991-001-a-2110">Opinion</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-2120">of</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-2130">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A51991-001-a-2140">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A51991-001-a-2150">hath</w>
     <w lemma="hitherto" pos="av" xml:id="A51991-001-a-2160">hitherto</w>
     <w lemma="represent" pos="vvn" xml:id="A51991-001-a-2170">represented</w>
     <w lemma="those" pos="d" xml:id="A51991-001-a-2180">those</w>
     <w lemma="unhappy" pos="j" xml:id="A51991-001-a-2190">unhappy</w>
     <w lemma="design" pos="n2" xml:id="A51991-001-a-2200">Designs</w>
     <pc xml:id="A51991-001-a-2210">,</pc>
     <w lemma="which" pos="crq" xml:id="A51991-001-a-2220">which</w>
     <w lemma="inconsiderate" pos="j" xml:id="A51991-001-a-2230">inconsiderate</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-2240">and</w>
     <w lemma="self-interested" pos="n1" xml:id="A51991-001-a-2250">self-interested</w>
     <w lemma="man" pos="n2" xml:id="A51991-001-a-2260">Men</w>
     <w lemma="have" pos="vvb" xml:id="A51991-001-a-2270">have</w>
     <w lemma="frame" pos="vvn" xml:id="A51991-001-a-2280">framed</w>
     <w lemma="against" pos="acp" xml:id="A51991-001-a-2290">against</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-2300">Your</w>
     <w lemma="majesty" pos="ng1" xml:id="A51991-001-a-2310">Majesty's</w>
     <w lemma="true" pos="j" xml:id="A51991-001-a-2320">true</w>
     <w lemma="interest" pos="n1" xml:id="A51991-001-a-2330">Interest</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-2340">and</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-2350">the</w>
     <w lemma="protestant" pos="jnn" xml:id="A51991-001-a-2360">Protestant</w>
     <w lemma="religion" pos="n1" xml:id="A51991-001-a-2370">Religion</w>
     <pc unit="sentence" xml:id="A51991-001-a-2380">.</pc>
     <w lemma="but" pos="acp" xml:id="A51991-001-a-2390">But</w>
     <w lemma="as" pos="acp" xml:id="A51991-001-a-2400">as</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-2410">I</w>
     <w lemma="can" pos="vmb" xml:id="A51991-001-a-2420">can</w>
     <w lemma="no" pos="avx-d" xml:id="A51991-001-a-2430">no</w>
     <w lemma="long" pos="avc-j" xml:id="A51991-001-a-2440">longer</w>
     <w lemma="join" pos="vvi" reg="join" xml:id="A51991-001-a-2450">joyn</w>
     <w lemma="with" pos="acp" xml:id="A51991-001-a-2460">with</w>
     <w lemma="such" pos="d" xml:id="A51991-001-a-2470">such</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-2480">to</w>
     <w lemma="give" pos="vvi" xml:id="A51991-001-a-2490">give</w>
     <w lemma="a" pos="d" xml:id="A51991-001-a-2500">a</w>
     <w lemma="pretence" pos="n1" xml:id="A51991-001-a-2510">pretence</w>
     <w lemma="by" pos="acp" xml:id="A51991-001-a-2520">by</w>
     <w lemma="conquest" pos="n1" xml:id="A51991-001-a-2530">Conquest</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-2540">to</w>
     <w lemma="bring" pos="vvi" xml:id="A51991-001-a-2550">bring</w>
     <w lemma="they" pos="pno" xml:id="A51991-001-a-2560">them</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-2570">to</w>
     <w lemma="effect" pos="vvi" xml:id="A51991-001-a-2580">effect</w>
     <pc xml:id="A51991-001-a-2590">,</pc>
     <w lemma="so" pos="av" xml:id="A51991-001-a-2600">so</w>
     <w lemma="will" pos="vmb" xml:id="A51991-001-a-2610">will</w>
     <w lemma="i" pos="pns" xml:id="A51991-001-a-2620">I</w>
     <w lemma="always" pos="av" xml:id="A51991-001-a-2630">always</w>
     <w lemma="with" pos="acp" xml:id="A51991-001-a-2640">with</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-2650">the</w>
     <w lemma="hazard" pos="n1" xml:id="A51991-001-a-2660">hazard</w>
     <w lemma="of" pos="acp" xml:id="A51991-001-a-2670">of</w>
     <w lemma="my" pos="po" xml:id="A51991-001-a-2680">my</w>
     <w lemma="life" pos="n1" xml:id="A51991-001-a-2690">Life</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-2700">and</w>
     <w lemma="fortune" pos="n1" xml:id="A51991-001-a-2710">Fortune</w>
     <pc join="right" xml:id="A51991-001-a-2720">(</pc>
     <w lemma="so" pos="av" xml:id="A51991-001-a-2730">so</w>
     <w lemma="much" pos="d" xml:id="A51991-001-a-2740">much</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-2750">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A51991-001-a-2760">Majesties</w>
     <w lemma="due" pos="j" xml:id="A51991-001-a-2770">due</w>
     <pc xml:id="A51991-001-a-2780">)</pc>
     <w lemma="endeavour" pos="vvb" xml:id="A51991-001-a-2790">endeavour</w>
     <w lemma="to" pos="prt" xml:id="A51991-001-a-2800">to</w>
     <w lemma="preserve" pos="vvi" xml:id="A51991-001-a-2810">preserve</w>
     <w lemma="your" pos="po" xml:id="A51991-001-a-2820">Your</w>
     <w lemma="royal" pos="j" xml:id="A51991-001-a-2830">Royal</w>
     <w lemma="person" pos="n1" xml:id="A51991-001-a-2840">Person</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-2850">and</w>
     <w lemma="lawful" pos="j" xml:id="A51991-001-a-2860">Lawful</w>
     <w lemma="right" pos="n2-j" xml:id="A51991-001-a-2870">Rights</w>
     <w lemma="with" pos="acp" xml:id="A51991-001-a-2880">with</w>
     <w lemma="all" pos="d" xml:id="A51991-001-a-2890">all</w>
     <w lemma="the" pos="d" xml:id="A51991-001-a-2900">the</w>
     <w lemma="tender" pos="j" xml:id="A51991-001-a-2910">tender</w>
     <w lemma="concern" pos="vvi" xml:id="A51991-001-a-2920">Concern</w>
     <w lemma="and" pos="cc" xml:id="A51991-001-a-2930">and</w>
     <w lemma="dutiful" pos="j" xml:id="A51991-001-a-2940">dutiful</w>
     <w lemma="respect" pos="n1" xml:id="A51991-001-a-2950">Respect</w>
     <w lemma="that" pos="cs" xml:id="A51991-001-a-2960">that</w>
     <w lemma="become" pos="vvz" xml:id="A51991-001-a-2970">becomes</w>
     <pc xml:id="A51991-001-a-2980">,</pc>
     <w lemma="sir" pos="n1" xml:id="A51991-001-a-2990">Sir</w>
     <pc xml:id="A51991-001-a-3000">,</pc>
    </p>
    <closer xml:id="A51991-e90">
     <signed xml:id="A51991-e100">
      <w lemma="your" pos="po" xml:id="A51991-001-a-3010">Your</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A51991-001-a-3020">Majesties</w>
      <w lemma="most" pos="avs-d" xml:id="A51991-001-a-3030">Most</w>
      <w lemma="dutiful" pos="j" xml:id="A51991-001-a-3040">dutiful</w>
      <w lemma="and" pos="cc" xml:id="A51991-001-a-3050">and</w>
      <w lemma="most" pos="avs-d" xml:id="A51991-001-a-3060">most</w>
      <w lemma="oblige" pos="j-vn" xml:id="A51991-001-a-3070">obliged</w>
      <w lemma="subject" pos="n1" xml:id="A51991-001-a-3080">Subject</w>
      <w lemma="and" pos="cc" xml:id="A51991-001-a-3090">and</w>
      <w lemma="servant" pos="n1" xml:id="A51991-001-a-3100">Servant</w>
      <pc unit="sentence" xml:id="A51991-001-a-3110">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
